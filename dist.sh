#! /usr/bin/env bash

# Create ODB source distributions.
#
# -rebuild
# -complete  package/build libcutl and the ODB compiler; implies -rebuild
# -test  build and run tests and examples
# -db <database>  only configure, test, build for <database> [default is all]
# -odb <odb-compiler-path>
# -cc <c-compiler>
# -cxx <c++-compiler>
# -cxxflags <c++-compiler-flags>
# -ccp <gcc-plugin-compiler>
# -cxxp <g++-plugin-compiler>
# -cxxpflags <g++-plugin-compiler-flags>
# -<db>-options <options>
# -<db>-test-options <options>
# -j <jobs> [default is 8]
#
trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

# Find output directory for a project. It is either $1-default or if
# that doesn't exist, it is $1.
#
function find_out_dir ()
{
  # Get the actual directory in case -default is a symlink.
  #
  tmp=`realpath $1-default 2>/dev/null`

  if [ -n "$tmp" -a -d "$tmp" ]; then
    echo "$tmp"
  else
    echo "$1"
  fi
}

wd=`pwd`
out_root=/tmp
src_root=$HOME/work/odb
ver=`sed -e 's/^\([^.]*\)\.\([^.]*\)\..*$/\1.\2/' $src_root/odb/version`

test=n
rebuild=n
complete=n
db=
odb=$src_root/odb/odb/odb
cc=gcc
cxx=g++
ccp=gcc
cxxp=g++
cxxflags="-W -Wall -Wno-unknown-pragmas -O3"
cxxpflags="-W -Wall -O3"
jobs=8

mysql_options=
mysql_test_options=
sqlite_options=
sqlite_test_options=
pgsql_options=
pgsql_test_option=
oracle_options=
oracle_test_options=
mssql_options=
mssql_test_options=

while [ $# -gt 0 ]; do
  case $1 in
    -rebuild)
      rebuild=y
      shift
      ;;
    -complete)
      rebuild=y
      complete=y
      shift
      ;;
    -test)
      test=y
      shift
      ;;
    -db)
      shift
      db="$db $1"
      shift
      ;;
    -odb)
      shift
      odb=$1
      shift
      ;;
    -cc)
      shift
      cc=$1
      shift
      ;;
    -cxx)
      shift
      cxx=$1
      shift
      ;;
    -cxxflags)
      shift
      cxxflags=$1
      shift
      ;;
    -ccp)
      shift
      ccp=$1
      shift
      ;;
    -cxxp)
      shift
      cxxp=$1
      shift
      ;;
    -cxxpflags)
      shift
      cxxpflags=$1
      shift
      ;;
    -mysql-options)
      shift
      mysql_options=$1
      shift
      ;;
    -mysql-test-options)
      shift
      mysql_test_options=$1
      shift
      ;;
    -sqlite-options)
      shift
      sqlite_options=$1
      shift
      ;;
    -sqlite-test-options)
      shift
      sqlite_test_options=$1
      shift
      ;;
    -pgsql-options)
      shift
      pgsql_options=$1
      shift
      ;;
    -pgsql-test-options)
      shift
      pgsql_test_options=$1
      shift
      ;;
    -oracle-options)
      shift
      oracle_options=$1
      shift
      ;;
    -oracle-test-options)
      shift
      oracle_test_options=$1
      shift
      ;;
    -mssql-options)
      shift
      mssql_options=$1
      shift
      ;;
    -mssql-test-options)
      shift
      mssql_test_options=$1
      shift
      ;;
    -j)
      shift
      jobs=$1
      shift
      ;;
    *)
      error "unknown option: $1"
      exit 1
      ;;
  esac
done

if [ "$db" = "" ]; then
  db="mssql oracle pgsql sqlite mysql"
fi

# Clean everything up if we are rebuilding.
#
if [ $rebuild = y ]; then
  rm -rf $out_root/libodb/*
  rm -rf $out_root/libodb-mssql/*
  rm -rf $out_root/libodb-oracle/*
  rm -rf $out_root/libodb-pgsql/*
  rm -rf $out_root/libodb-sqlite/*
  rm -rf $out_root/libodb-mysql/*
  rm -rf $out_root/libodb-boost/*
  rm -rf $out_root/libodb-qt/*
  rm -rf $out_root/odb-tests/* $out_root/odb-tests-*
  rm -rf $out_root/odb-examples/* $out_root/odb-examples-*

  if [ $complete = y ]; then
    rm -rf $out_root/libcutl/*
    rm -rf $out_root/odb/*
  fi

  rm -rf $out_root/pack
fi

mkdir -p $out_root/libodb
mkdir -p $out_root/libodb-mssql
mkdir -p $out_root/libodb-oracle
mkdir -p $out_root/libodb-pgsql
mkdir -p $out_root/libodb-sqlite
mkdir -p $out_root/libodb-mysql
mkdir -p $out_root/libodb-boost
mkdir -p $out_root/libodb-qt
mkdir -p $out_root/odb-tests
mkdir -p $out_root/odb-examples

if [ $complete = y ]; then
  mkdir -p $out_root/libcutl
  mkdir -p $out_root/odb
fi

mkdir -p $out_root/pack

# Build libcutl
#
if [ $complete = y ]; then
  cd $src_root/../cutl
  ./dist.sh -cc "$ccp" -cxx "$cxxp" -cxxflags "$cxxpflags" \
-out "$out_root/libcutl" -no-check -j $jobs
  cd $wd
fi

# Build ODB compiler
#
if [ $complete = y ]; then
  make -C `find_out_dir $src_root/odb` -f $src_root/odb/makefile \
dist dist_prefix=$out_root/odb

  cd $out_root/odb

  ./bootstrap
  ./configure --with-libcutl=../libcutl CC="$ccp" CXX="$cxxp" \
CXXFLAGS="$cxxpflags"

  make -j $jobs
  make dist

  cd $wd

  cp $out_root/odb/odb-$ver.*.zip $out_root/pack/
  cp $out_root/odb/odb-$ver.*.tar.gz $out_root/pack/
  cp $out_root/odb/odb-$ver.*.tar.bz2 $out_root/pack/

  odb=$out_root/odb/odb/odb
fi

# Build libodb
#
make -C `find_out_dir $src_root/libodb` -f $src_root/libodb/makefile \
dist dist_prefix=$out_root/libodb

cd $out_root/libodb

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure CC="$cc" CXX="$cxx" CXXFLAGS="$cxxflags"
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb/libodb-$ver.*.zip $out_root/pack/
cp $out_root/libodb/libodb-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb/libodb-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-mssql
#
make -C `find_out_dir $src_root/libodb-mssql` \
-f $src_root/libodb-mssql/makefile dist dist_prefix=$out_root/libodb-mssql

cd $out_root/libodb-mssql

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb \
              CC="$cc" CXX="$cxx" \
              CXXFLAGS="$cxxflags" \
              $mssql_options
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-mssql/libodb-mssql-$ver.*.zip $out_root/pack/
cp $out_root/libodb-mssql/libodb-mssql-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-mssql/libodb-mssql-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-oracle
#
make -C `find_out_dir $src_root/libodb-oracle` \
-f $src_root/libodb-oracle/makefile dist dist_prefix=$out_root/libodb-oracle

cd $out_root/libodb-oracle

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb \
              CC="$cc" CXX="$cxx" \
              CXXFLAGS="$cxxflags" \
              $oracle_options
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-oracle/libodb-oracle-$ver.*.zip $out_root/pack/
cp $out_root/libodb-oracle/libodb-oracle-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-oracle/libodb-oracle-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-pgsql
#
make -C `find_out_dir $src_root/libodb-pgsql` \
-f $src_root/libodb-pgsql/makefile dist dist_prefix=$out_root/libodb-pgsql

cd $out_root/libodb-pgsql

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb \
              CC="$cc" CXX="$cxx" \
              CXXFLAGS="$cxxflags" \
              $pgsql_options
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-pgsql/libodb-pgsql-$ver.*.zip $out_root/pack/
cp $out_root/libodb-pgsql/libodb-pgsql-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-pgsql/libodb-pgsql-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-sqlite
#
make -C `find_out_dir $src_root/libodb-sqlite` \
  -f $src_root/libodb-sqlite/makefile dist dist_prefix=$out_root/libodb-sqlite

cd $out_root/libodb-sqlite

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb \
              CC="$cc" CXX="$cxx" \
              CXXFLAGS="$cxxflags" \
              $sqlite_options
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-sqlite/libodb-sqlite-$ver.*.zip $out_root/pack/
cp $out_root/libodb-sqlite/libodb-sqlite-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-sqlite/libodb-sqlite-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-mysql
#
make -C `find_out_dir $src_root/libodb-mysql` \
-f $src_root/libodb-mysql/makefile dist dist_prefix=$out_root/libodb-mysql

cd $out_root/libodb-mysql

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb \
              CC="$cc" CXX="$cxx" \
              CXXFLAGS="$cxxflags" \
              $mysql_options
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-mysql/libodb-mysql-$ver.*.zip $out_root/pack/
cp $out_root/libodb-mysql/libodb-mysql-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-mysql/libodb-mysql-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-boost
#
make -C `find_out_dir $src_root/libodb-boost` \
-f $src_root/libodb-boost/makefile dist dist_prefix=$out_root/libodb-boost

cd $out_root/libodb-boost

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb CC="$cc" CXX="$cxx" CXXFLAGS="$cxxflags"
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-boost/libodb-boost-$ver.*.zip $out_root/pack/
cp $out_root/libodb-boost/libodb-boost-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-boost/libodb-boost-$ver.*.tar.bz2 $out_root/pack/

# Build libodb-qt
#
make -C `find_out_dir $src_root/libodb-qt` \
-f $src_root/libodb-qt/makefile dist dist_prefix=$out_root/libodb-qt

cd $out_root/libodb-qt

if [ $rebuild = y -o ! -f Makefile ]; then
  ./bootstrap
  ./configure --with-libodb=../libodb CC="$cc" CXX="$cxx" CXXFLAGS="$cxxflags"
fi

make -j $jobs
make dist

cd $wd

cp $out_root/libodb-qt/libodb-qt-$ver.*.zip $out_root/pack/
cp $out_root/libodb-qt/libodb-qt-$ver.*.tar.gz $out_root/pack/
cp $out_root/libodb-qt/libodb-qt-$ver.*.tar.bz2 $out_root/pack/

# Build odb-tests
#
make -C `find_out_dir $src_root/tests-mysql` -f $src_root/tests/makefile dist \
dist_prefix=$out_root/odb-tests

cd $out_root/odb-tests

if [ $rebuild = y -o ! -f configure ]; then
  ./bootstrap
fi

cd $wd

dist_tests=

for d in $db; do
  mkdir -p $out_root/odb-tests-$d
  cd $out_root/odb-tests-$d

  optvar=${d}_test_options

  if [ $rebuild = y -o ! -f Makefile ]; then
    ../odb-tests/configure \
--with-database=$d \
--with-libodb=../libodb \
--with-libodb-$d=../libodb-$d \
--with-libodb-boost=../libodb-boost \
--with-libodb-qt=../libodb-qt \
${!optvar} \
CC="$cc" CXX="$cxx" \
CXXFLAGS="$cxxflags" \
ODB=$odb
  fi

  if [ $test = y ]; then
    make -j $jobs
    make check
  fi

  if [ "$dist_tests" = "" ]; then
    dist_tests=$d
    make dist
  fi

  cd $wd
done

cp $out_root/odb-tests-$dist_tests/odb-tests-$ver.*.zip $out_root/pack/
cp $out_root/odb-tests-$dist_tests/odb-tests-$ver.*.tar.gz $out_root/pack/
cp $out_root/odb-tests-$dist_tests/odb-tests-$ver.*.tar.bz2 $out_root/pack/


# Build odb-examples
#
make -C `find_out_dir $src_root/examples-mysql` \
-f $src_root/examples/makefile dist dist_prefix=$out_root/odb-examples

cd $out_root/odb-examples

if [ $rebuild = y -o ! -f configure ]; then
  ./bootstrap
fi

cd $wd

dist_examples=

for d in $db; do
  mkdir -p $out_root/odb-examples-$d
  cd $out_root/odb-examples-$d

  optvar=${d}_test_options

  if [ $rebuild = y -o ! -f Makefile ]; then
    ../odb-examples/configure \
--with-database=$d \
--with-libodb=../libodb \
--with-libodb-$d=../libodb-$d \
--with-libodb-boost=../libodb-boost \
--with-libodb-qt=../libodb-qt \
${!optvar} \
CC="$cc" CXX="$cxx" \
CXXFLAGS="$cxxflags" \
ODB=$odb
  fi

  if [ $test = y ]; then
    make -j $jobs
    make check
  fi

  if [ "$dist_examples" = "" ]; then
    dist_examples=$d
    make dist
  fi

  cd $wd
done

cp $out_root/odb-examples-$dist_examples/odb-examples-$ver.*.zip $out_root/pack/
cp $out_root/odb-examples-$dist_examples/odb-examples-$ver.*.tar.gz $out_root/pack/
cp $out_root/odb-examples-$dist_examples/odb-examples-$ver.*.tar.bz2 $out_root/pack/
