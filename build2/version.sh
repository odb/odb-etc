#!/bin/bash

# Usage: run from the ODB repository root.
#
# Recommended procedure:
#
# ./version.sh
# git diff # review (compare to previous release for any discrepancies)
# git ci -a -m "Release version X.Y.Z"
# git tag -a "vX.Y.Z" -m "Tag version X.Y.Z"
#

# @@ Fix libcutl and libstudxml version upon release if betas for LTS
#    and restore when re-opening for development. Otherwise, update
#    constraints to latest released.
# @@ CLI dependency is managed manually.
# @@ MySQL, PostgreSQL client dependencies are managed manually (tests, examples)
# @@ Version/date/copyright in odb/doc/{*-prologuie,manual.xhtml,manual.html2ps} -
#    grep for 2.5, [cC]opyright.

# Currently we use even beta numbers for snapshot version meaning
# after the release b.1 becomes b.2.z, not b.1.z and at release b.2.z
# becomes b.3.
#
#interface_num=20479
interface_num=20500

#interface_str=2.5-b.29
interface_str=2.5

#num=2049979
num=2050000

#str=2.5.0-b.29
str=2.5.0

#man_str=2.5.0-b.29
#man_str=2.5.0-b.30.z
#man_str=2.5.0-b.29+1
man_str=2.5.0

#profile_num=2047900
profile_num=2050000

#profile_str=2.5.0-b.29
profile_str=2.5.0

# No max, always >=.
#
# We should try to stay at least one version behind not to cause unnecessary
# inconvenience.
#
build2=0.17.0
bpkg=0.17.0

#---------------------------------------------------------------------------

mj="$(echo "$man_str" | sed -re 's/([^.]+)\..*/\1/')"
mn="$(echo "$man_str" | sed -re 's/[^.]+\.([^.]+)\..*/\1/')"
pt="$(echo "$man_str" | sed -re 's/[^.]+\.[^.]+\.([^.-]+).*/\1/')"
ab="$(echo "$man_str" | sed -n -re 's/[^.]+\.[^.]+\.[^.-]+-([ab]).*/\1/p')"
pr="$(echo "$man_str" | sed -n -re 's/[^.]+\.[^.]+\.[^.-]+-[ab]\.([^.+]+).*/\1/p')"

# In manifests for alpha/beta we use an exact match. For snapshots -- snapshot
# range. Otherwise an X.Y.* range.
#
if [ -n "$ab" ]; then
  if [[ $man_str == *.z ]]; then
    man="[$mj.$mn.$pt-$ab.$pr.1 $mj.$mn.$pt-$ab.$(($pr+1)))"
  else
    man="== $man_str"
  fi
else
  man="[$mj.$mn.0 $mj.$(($mn+1)).0-); $mj.$mn.X"
fi

cd libodb

sed -i -e "s/^\(#define ODB_VERSION     \).*/\1$interface_num/"     odb/version.hxx.in
sed -i -e "s/^\(#define ODB_VERSION_STR \).*/\1\"$interface_str\"/" odb/version.hxx.in
sed -i -e "s/^\(#define LIBODB_VERSION \).*/\1$num/"                odb/version.hxx.in

sed -i -e "s/^\(version:\).*/\1 $man_str/"              manifest
sed -i -e "s/^\(depends: \* build2\) .*/\1 >= $build2/" manifest
sed -i -e "s/^\(depends: \* bpkg\) .*/\1 >= $bpkg/"     manifest
cd ..

for db in mysql sqlite pgsql oracle mssql; do
  DB=`echo $db | sed -e 's/\(.*\)/\U\1/'`
  cd libodb-$db

  sed -i -e "s/^\(#define LIBODB_${DB}_VERSION \).*/\1$num/" odb/$db/version.hxx.in

  cd ..
done

for pr in boost qt; do
  PR=`echo $pr | sed -e 's/\(.*\)/\U\1/'`
  cd libodb-$pr

  sed -i -e "s/^\(#define ODB_${PR}_VERSION     \).*/\1$profile_num/"     odb/$pr/version.hxx.in
  sed -i -e "s/^\(#define ODB_${PR}_VERSION_STR \).*/\1\"$profile_str\"/" odb/$pr/version.hxx.in
  sed -i -e "s/^\(#define LIBODB_${PR}_VERSION \).*/\1$num/"              odb/$pr/version.hxx.in

  sed -i -e "s/^\(--hxx-prologue '#if ODB_${PR}_VERSION != \).*/\1$profile_num \/\/ $profile_str'/" odb/$pr/version.options

  cd ..
done

for l in sqlite pgsql mysql oracle mssql boost qt; do
  cd libodb-$l

  sed -i -e "s/^\(version:\).*/\1 $man_str/"              manifest
  sed -i -e "s/^\(depends: \* build2\) .*/\1 >= $build2/" manifest
  sed -i -e "s/^\(depends: \* bpkg\) .*/\1 >= $bpkg/"     manifest

  cd ..
done

for t in tests examples; do
  cd odb-$t

  sed -i -e "s/^\(version:\).*/\1 $man_str/"              manifest
  sed -i -e "s/^\(depends: \* build2\) .*/\1 >= $build2/" manifest
  sed -i -e "s/^\(depends: \* bpkg\) .*/\1 >= $bpkg/"     manifest

  cd ..
done

cd odb
sed -i -e "s/^\(#define ODB_VERSION \).*/\1$interface_num/"    odb/version.hxx.in
sed -i -e "s/^\(#define ODB_COMPILER_VERSION_OLD \).*/\1$num/" odb/version.hxx.in

sed -i -e "s/^\(version:\).*/\1 $man_str/"              manifest
sed -i -e "s/^\(depends: \* build2\) .*/\1 >= $build2/" manifest
sed -i -e "s/^\(depends: \* bpkg\) .*/\1 >= $bpkg/"     manifest
cd ..
