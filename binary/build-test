#! /usr/bin/env bash

# Build and run ODB tests using a precompiled binary.
#
# Usage: build [options] <config>
#
# <config> is the script that sets up the configuration.
#
# -rebuild
# -test
# -db <database>
# -j
#
trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

function unpack ()
{
  rm -rf $1-$mver.*/
  bzip2 -dc $1-$mver.*.tar.bz2 | $tar xf -
}

# Use gtar if agailable
#
if type -P gtar &>/dev/null; then
  tar=gtar
else
  tar=tar
fi

test=n
rebuild=n
jobs=1
db=

while [ $# -gt 0 ]; do
  case $1 in
    -rebuild)
      rebuild=y
      shift
      ;;
    -test)
      test=y
      shift
      ;;
    -db)
      shift
      db="$db $1"
      shift
      ;;
    -j)
      shift
      jobs=$1
      shift
      ;;
    -*)
      error "unknown option: $1"
      exit 1
      ;;
    *)
      break
      ;;
  esac
done

if [ "$1" = "" ]; then
  error "Usage: $0 [options] config"
  exit 1
fi

if [ "$db" = "" ]; then
  db="oracle pgsql sqlite mysql"

  if [ "`uname`" = "Linux" ]; then
    db="mssql $db"
  fi
fi

# Read in the config file.
#
source $1

#
#
wd=`pwd`
ver=`echo odb-*-*-*.tar.bz2 | sed -e "s%odb-\([^-]*\).*.tar.bz2%\1%"`
mver=`echo $ver | sed -e 's%\([0-9]*\.[0-9]*\).*%\1%'`
arch=`echo odb-$ver-*.tar.bz2 | sed -e "s%odb-$ver-\(.*\).tar.bz2%\1%"`

# Clean and unpack everything up if we are rebuilding.
#
if [ $rebuild = y ]; then
  unpack libodb

  for d in $db; do
    unpack libodb-$d
  done

  unpack libodb-boost
  unpack libodb-qt
  unpack odb-tests
  unpack odb-examples

  bzip2 -dc odb-$ver-$arch.tar.bz2 | $tar xf -

  for d in $db; do
    rm -rf odb-tests-$d
    rm -rf odb-examples-$d
  done
fi

# Build libodb
#
libodb=`echo libodb-$mver.*/`
cd $libodb

if [ $rebuild = y -o ! -f Makefile ]; then
  ./configure \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS"
fi

make -j $jobs
cd ..

# Build libodb-<db>
#
for d in $db; do
  cd libodb-$d-$mver.*/

  optvar=${d}_build_options

  if [ $rebuild = y -o ! -f Makefile ]; then
    ./configure \
--with-libodb=../$libodb \
${!optvar} \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS"
  fi

  make -j $jobs
  cd ..
done

# Build libodb-boost
#
libodb_boost=`echo libodb-boost-$mver.*/`

if [ "$with_boost" = "y" ]; then
  cd $libodb_boost

  if [ $rebuild = y -o ! -f Makefile ]; then
    ./configure \
--with-libodb=../$libodb \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS"
  fi

  make -j $jobs
  cd ..
fi

# Build libodb-qt
#
libodb_qt=`echo libodb-qt-$mver.*/`

if [ "$with_qt" = "y" ]; then
  cd $libodb_qt

  if [ $rebuild = y -o ! -f Makefile ]; then
    ./configure \
--with-libodb=../$libodb \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS"
  fi

  make -j $jobs
  cd ..
fi

# Build odb-tests
#
for d in $db; do
  mkdir -p odb-tests-$d
  cd odb-tests-$d

  optvar=${d}_use_options

  if [ $rebuild = y -o ! -f Makefile ]; then

    libodb_db=`echo ../libodb-$d-$mver.*/`

    ../odb-tests-$mver.*/configure \
--with-database=$d \
--with-libodb=../$libodb \
--with-libodb-$d=$libodb_db \
--with-libodb-boost=../$libodb_boost \
--with-libodb-qt=../$libodb_qt \
${!optvar} \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS" \
ODB=$wd/odb-$ver-$arch/bin/odb

  fi

  make -j $jobs

  if [ $test = y ]; then
    make check
  fi

  cd ..
done

# Build odb-examples
#
for d in $db; do
  mkdir -p odb-examples-$d
  cd odb-examples-$d

  optvar=${d}_use_options

  if [ $rebuild = y -o ! -f Makefile ]; then

    libodb_db=`echo ../libodb-$d-$mver.*/`

    ../odb-examples-$mver.*/configure \
--with-database=$d \
--with-libodb=../$libodb \
--with-libodb-$d=$libodb_db \
--with-libodb-boost=../$libodb_boost \
--with-libodb-qt=../$libodb_qt \
${!optvar} \
CC="$CC" \
CXX="$CXX" \
CPPFLAGS="$CPPFLAGS" \
CXXFLAGS="$CXXFLAGS" \
LDFLAGS="$LDFLAGS" \
ODB=$wd/odb-$ver-$arch/bin/odb
  fi

  make -j $jobs

  if [ $test = y ]; then
    make check
  fi

  cd ..
done
