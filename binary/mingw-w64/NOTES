Notes:

- Currently have to use the mingw/lib -> i686-w64-mingw32/lib64 hack to
  make GCC see 64-bit system libraries. This is required for both cross
  and native compiler. In the native compiler it is removed later and
  the result seem to work ok.

- If updating msys, add gendef and reimp (from mingw-utils) utilities.

An overview of steps required to switch to a new GCC version. This is
a complete toolchain bring up.

1. Get the MinGW-W64 GCC build of this version and get configuration
   flags (gcc -v). The idea is to build both the cross-compiler
   and the native compiler (with linked-in ODB plugin) as close
   to the MinGW-W64 configuration as possible.

2. Build cross-binutils:

   1. Move old cross/mingw to cross/mingw-X.Y.Z.
   2. Unpack binutils, rename to binutils, create or clean binutils-cross-build.
   3. Build and install:

      cd binutils-cross-build
      ../binutils-cross-configure
      make -j 16
      make install

3  Install runtime headers:

   1. Unpack mingw-w64 and rename to mingw-w64, create or clean
      mingw-w64-headers-build.
   2. Configure & install:

      cd mingw-w64-headers-build
      ../rt-headers-cross-configure
      make install

4. Build cross-GCC, phase 1:

   1. Unpack GCC, rename to gcc-cross, create or clean gcc-cross-build.
   2. Adapt older gcc-X.Y.Z-mingw-cross.patch (--dry-run) and apply.
   3. Adapt gcc-cross-configure based on MinGW-W64 config and old config.
   4. Build & install GCC only:

      # High -j does not seem to work properly, -j 4 seems to work.

      cd gcc-cross-build
      ../gcc-cross-configure
      make -j 4 all-gcc
      make install-gcc

5. Build runtime:

   1. Create or clean mingw-w64-build.
   2. Build & install:

      export PATH=`pwd`/cross/mingw/bin:$PATH
      cd mingw-w64-build
      ../rt-cross-configure
      make -j 22
      make -j 22 install

      i686-w64-mingw32-ar cr ../cross/mingw/i686-w64-mingw32/lib/libpthread.a
      i686-w64-mingw32-ar cr ../cross/mingw/i686-w64-mingw32/lib64/libpthread.a

      mkdir winpthreads32
      cd winpthreads32
      ../../pthread-cross-configure
      make -j 16
      make install
      mv ../../cross/mingw/i686-w64-mingw32/bin/libwinpthread* ../../cross/mingw/i686-w64-mingw32/lib/

      cd ..
      mkdir winpthreads64
      cd winpthreads64
      ../../pthread-cross-configure-64
      make -j 16
      make install
      mv ../../cross/mingw/i686-w64-mingw32/bin/libwinpthread* ../../cross/mingw/i686-w64-mingw32/lib64/

7. Build cross-GCC, phase 2:

   1. Build & install:

      @@ Had to ln mingw/lib to lib64 in cross/mingw/

      cd gcc-cross-build
      make -j 16
      make install

   2. Disable .dll & .la files in i686-w64-mingw32/lib and bin/ (for
      static link)

   3. Check that the compiler works. Compile and run sizeof_funds.cxx and
      throw.cxx.

8. Package runtime:

   1. Save mingw-rt as mingw-rt-X.Y.Z

   2. Package:

      cd mingw-w64-headers-build
      make DESTDIR=/home/boris/work/odb/build/mingw-w64/mingw-rt install

      # May need export PATH above
      cd mingw-w64-build
      make DESTDIR=/home/boris/work/odb/build/mingw-w64/mingw-rt install

      cd winpthreads32
      make DESTDIR=/home/boris/work/odb/build/mingw-w64/mingw-rt install
      #do manually: mv ../../mingw-rt/i686-w64-mingw32/bin/libwinpthread* ../../mingw-rt/i686-w64-mingw32/lib/

      cd winpthreads64
      make DESTDIR=/home/boris/work/odb/build/mingw-w64/mingw-rt install
      #do manually: mv ../../mingw-rt/i686-w64-mingw32/bin/libwinpthread* ../../mingw-rt/i686-w64-mingw32/lib64/

      Move to the top level directory.

9. Build binutils:

   1. Create or clean binutils-build, /mingw.
   2. Build and install:

      export PATH=`pwd`/cross/mingw/bin:$PATH
      cd binutils-build
      ../configure-binutils
      make -j 16
      make install

   3. Copy /mingw/* to mingw-binutils.

10. Build GCC prerequisites (gmp, mpfr, mpc):

    1. Unpack {gmp, mpfr, mpc}
    2. Build and install:

      cd {gmp, mpfr, mpc}
      ../gcc-prereq-configure
      make -j 16
      make install

11. Build the native compiler:

  1. Unpack GCC, rename to gcc, create or clean gcc-build.
  2. Adapt older gcc-X.Y.Z-mingw.patch (--dry-run) and apply.
  3. Adapt older gcc-X.Y.Z-static-plugin.patch (--dry-run) and apply.
     This step will probably require careful review of changes in the
     plugin code (plugin.h and plugin.c).
  4. Regenerate configure script in gcc/gcc/. Use the same autoconf
     version as in the original (see ~/build/autocond-X.Y). For example:

     sudo make -C ~/build/autoconf-2.64 install
     autoconf --version
     cd gcc
     autoconf
     sudo make -C ~/build/autoconf-2.64 uninstall

  4. Adapt gcc-configure.
  5. Configure and make GCC manually:

     export PATH=`pwd`/cross/mingw/bin:$PATH
     cd gcc-build
     ../gcc-configure
     make -j 16

     This step will fail due to lack of libplugin-stub.a. That's
     ok. Copy .h and .def files from gcc-build/gcc/ to gcc-headers/
     (backup old ones).

4. Run the build-dist script with -rebuild

   1. If GCC build fails, then it can be re-configured or re-made
      directly from the gcc-build directory provided you don't
      clean it up (if you do so, you can manually copy libplugin-stub.a
      to gcc-build/gcc/).

   2. Check the package for any stray directories/files. Compare size to
      the old version.

      Check that 32 and 64-bit libstdc++ are different (use file) Globbing
      for odb.

   3. On Windows, check that:

      - No dependencies on MinGW DLLs (odb, g++, cc1plus, ld).

      - Compiler, including exceptions, work. Compile and run sizeof_funds.cxx
        and throw.cxx.

      - Make sure ODB runs, including throwing exceptions (run on object
        with private data members).

5. Copy updates/patches to odb/etc/.
