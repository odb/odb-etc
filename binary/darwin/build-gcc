#! /usr/bin/env bash

# Create GCC compiler Darwin distribution.
#
# -rebuild
# -arch
#
trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

arch=i686-apple-darwin10
rebuild=n

while [ $# -gt 0 ]; do
  case $1 in
    -rebuild)
      rebuild=y
      shift
      ;;
    -arch)
      shift
      arch=$1
      shift
      ;;
    *)
      error "unknown option: $1"
      exit 1
      ;;
  esac
done

out_root=`pwd`

mkdir -p gcc-build

# Clean everything up if we are rebuilding.
#
if [ $rebuild = y ]; then
  rm -rf gcc-build/*
  rm -rf /$arch/*
fi

# Build gcc
#
cd gcc-build

if [ $rebuild = y ]; then
  ../gcc-configure $arch
fi

make -j 2
make install
make -C $arch/libstdc++-v3 install-strip

cd ..

# Make a fat binary for libstdc++ (libgcc_s already seems to be fat).
#
cd /$arch/lib
mkdir -p i386
mv libstdc++.6.dylib i386/
lipo -create i386/libstdc++.6.dylib x86_64/libstdc++.6.dylib -output libstdc++.6.dylib

# Clean it up a bit.
#
rm -f /$arch/bin/$arch-*
