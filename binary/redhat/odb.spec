%ifarch i686
%define arch i686
%endif

%ifarch x86_64
%define arch x86_64
%endif

Summary: ORM for C++
Name: odb
Version: 2.4.0
Release: 1
License: GPLv2, GPLv3, LGPLv3
Group: Development/Tools
URL: http://www.codesynthesis.com/products/odb/
Source: %{name}-%{version}-%{arch}-linux-gnu.tar.bz2
Prefix: %{_prefix}
BuildRoot: %{_tmppath}/%{name}-root
AutoReqProv: no

%description
ODB is an object-relational mapping (ORM) system for C++. It provides
tools, APIs, and library support that allow you to persist C++ objects
to a relational database (RDBMS) without having to deal with tables,
columns, or SQL and without manually writing any of the mapping code.

%prep
%setup -q -n %{name}-%{version}-%{arch}-linux-gnu

%build

# We don't want the binaries to be stripped.
#
%ifarch x86_64
%define __os_install_post /usr/lib/rpm/brp-compress
%endif

%install

rm -rf ${RPM_BUILD_ROOT}

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_docdir}/odb
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/odb
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}

cp -r bin/* $RPM_BUILD_ROOT%{_bindir}/
cp -r doc/* $RPM_BUILD_ROOT%{_docdir}/odb/
cp -r libexec/* $RPM_BUILD_ROOT%{_libexecdir}/
cp -r man/* $RPM_BUILD_ROOT%{_mandir}/
cp -r README $RPM_BUILD_ROOT%{_docdir}/odb/

# Remove Debian-specific options.
#
head --lines=-6 etc/odb/default.options >$RPM_BUILD_ROOT%{_sysconfdir}/odb/default.options

%clean
rm -rf ${RPM_BUILD_ROOT}

%post

%preun

%files
%defattr(-,root,root)
%{_bindir}/odb
%{_libexecdir}/odb
%{_mandir}/man1/odb.1.gz
%doc %{_docdir}/odb/*
%config %{_sysconfdir}/odb/default.options

%changelog
* Thu Feb 10 2015 Boris Kolpackov <boris@codesynthesis.com>
  - new major release

* Wed Dec 10 2014 Boris Kolpackov <boris@codesynthesis.com>
  - new pre-release

* Thu Oct 24 2013 Boris Kolpackov <boris@codesynthesis.com>
  - new major release

* Fri Jun 21 2013 Boris Kolpackov <boris@codesynthesis.com>
  - new bugfix release

* Fri Feb 22 2013 Boris Kolpackov <boris@codesynthesis.com>
  - spec file written for odb binary package
