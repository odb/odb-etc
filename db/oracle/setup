$ su oracle
$ sqlplus / as sysdba

EXEC DBMS_XDB.SETLISTENERLOCALACCESS(FALSE);

alter profile DEFAULT limit password_life_time UNLIMITED;

CREATE USER odb_test
IDENTIFIED BY odb_test
DEFAULT TABLESPACE users
TEMPORARY TABLESPACE temp;

GRANT
CREATE TABLE,
CREATE SEQUENCE,
CREATE TRIGGER,
CREATE SESSION,
CREATE TYPE,
CREATE PROCEDURE,
UNLIMITED TABLESPACE TO odb_test;

=======
For some Oracle installations (e.g., non-XE), the directory with OCI
headers/libraries is not accessible unless the user is in the oinstall
group. To fix, add the user in question to this group and switch to it
(the last part is not ideal since all new files will be created with
this group):

usermod -g oinstall boris
newgrp oinstall

=======

If you are getting "ORA-12516: TNS:listener could not find available
handler with matching protocol stack" (this seems to be the case with
10.2 XE), you will need to increase the number of connections/processes:

col name format A30
col value format A30
select name, value from v$parameter where name in ('processes','sessions');
alter system set processes=300 scope=spfile;
alter system set sessions=300 scope=spfile;

Then restart the database (via init.d).

=======

If running an OCI-based client under valgrind you get "ORA-12546: TNS:
permission denied", then you need to set the TWO_TASK environment
variable:

export TWO_TASK=<service name>

For example:

export TWO_TASK=xe
