@echo off
rem file      : build.bat
rem license   : GNU GPL v2; see accompanying LICENSE file

rem
rem build.bat database qt-version vc-version conf plat [/Build|/Clean|/Rebuild]
rem
rem conf: {Debug,Release}|all
rem plat: {Win32,x64}|all
rem

setlocal

if "_%1_" == "__" (
  echo no database specified
  goto usage
)

if "_%2_" == "__" (
  echo no Qt version specified
  goto usage
)

if "_%3_" == "__" (
  echo no VC++ version specified
  goto usage
)

if "_%~4_" == "__" (
  echo no configuration specified
  goto usage
)

if "_%~5_" == "__" (
  echo no platform specified
  goto usage
)

set "failed="

if "_%2_" == "_0_" set "qtver=0"
if "_%2_" == "_4_" set "qtver=4"
if "_%2_" == "_5_" set "qtver=5"

if "_%qtver%_" == "__" (
  echo unknown Qt version %2
  goto usage
)

if "_%3_" == "_8_" set "vcver=8"
if "_%3_" == "_9_" set "vcver=9"
if "_%3_" == "_10_" set "vcver=10"
if "_%3_" == "_11_" set "vcver=11"
if "_%3_" == "_12_" set "vcver=12"

if "_%vcver%_" == "__" (
  echo unknown VC++ version %3
  goto usage
)

set "confs=%~4"
set "plats=%~5"

if "_%confs%_" == "_all_" set "confs=Debug Release"
if "_%plats%_" == "_all_" set "plats=Win32 x64"

set "action=%6"
if "_%action%_" == "__" set "action=/Build"

set "devenv=%DEVENV%"
if "_%devenv%_" == "__" set "devenv=devenv.com"

goto start

rem
rem %1 - solution name
rem %2 - configuration to build
rem %3 - platform to build
rem
:run_build
  echo.
  echo building %1 %3 %2
  "%devenv%" %1 %action% "%2|%3" 2>&1
  if errorlevel 1 set "failed=%failed% %1\%3\%2"
goto :eof

:start

for %%d in (libodb libodb-%1 libodb-boost) do (
  for %%c in (%confs%) do (
      for %%p in (%plats%) do (
        call :run_build %%d/%%d-vc%vcver%.sln %%c %%p
    )
  )
)

if not "_%failed%_" == "__" goto error

if "_%qtver%_" == "_0_" goto skip_libodb_qt
for %%d in (libodb-qt) do (
  for %%c in (%confs%) do (
      for %%p in (%plats%) do (
        call :run_build %%d/%%d%qtver%-vc%vcver%.sln %%c %%p
    )
  )
)
if not "_%failed%_" == "__" goto error
:skip_libodb_qt

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build odb-examples-%1/examples-%1-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build odb-examples-%1/boost/boost-%1-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

if "_%qtver%_" == "_0_" goto skip_qt_examples
for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build odb-examples-%1/qt/qt%qtver%-%1-vc%vcver%.sln %%c %%p
  )
)
if not "_%failed%_" == "__" goto error
:skip_qt_examples

cd odb-tests-%1
call build.bat %1 %3 %4 %5 %action%
if errorlevel 1 (
  cd ..
  goto error
)

cd boost
call build.bat %1 %3 %4 %5 %action%
if errorlevel 1 (
  cd ..\..
  goto error
)
cd ..

if "_%qtver%_" == "_0_" goto skip_qt_tests
cd qt
call build.bat %1 %2 %3 %4 %5 %action%
if errorlevel 1 (
  cd ..\..
  goto error
)
cd ..
:skip_qt_tests

echo.
echo ALL BUILDS SUCCEEDED
echo.
goto end

:usage
echo.
echo usage: build.bat database qt-version vc-version conf plat [action]
echo   valid configurations are: {Debug,Release}|all
echo   valid platforms are: {Win32,x64}|all
echo   valid actions are: /Build (default), /Clean, and /Rebuild
echo.

:error
if not "_%failed%_" == "__" (
  echo.
  for %%t in (%failed%) do echo FAILED: %%t
  echo.
)
endlocal
exit /b 1

:end
endlocal
