@echo off

if "_%1_" == "__" (
  echo no VC++ version specified
  goto usage
)

rem set CL=/DMSSQL_SERVER_VERSION=900

set ODB=c:\projects\odb
set MYSQL=c:\projects\mysql
set MYSQL64=c:\projects\mysql64
set SQLITE=c:\projects\sqlite-vc%1.0
set PGSQL=c:\projects\pgsql
set PGSQL64=c:\projects\pgsql64
set ORACLE=c:\projects\oracle
set ORACLE64=c:\projects\oracle64
set QTCORE=c:\projects\qtcore-vc%1.0
set QTCORE64=c:\projects\qtcore64-vc%1.0
rem set "NLS_LANG=AMERICA_AMERICA.WE8MSWIN1252"
set "DIFF=c:\cygwin\bin\diff.exe -ubB"
if "_%1_" == "_8_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.com"
if "_%1_" == "_9_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.com"
if "_%1_" == "_10_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.com"
if "_%1_" == "_11_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.com"
if "_%1_" == "_12_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.com"
set PATH=%ODB%\libodb\bin;%ODB%\libodb-sqlite\bin;%ODB%\libodb-mysql\bin;%ODB%\libodb-pgsql\bin;%ODB%\libodb-oracle\bin;%ODB%\libodb-mssql\bin;%ODB%\libodb-boost\bin;%ODB%\libodb-qt\bin;%MYSQL%\bin;%SQLITE%\bin;%PGSQL%\bin;%ORACLE%;%QTCORE%\bin;%PATH%
set PATH=%ODB%\libodb\bin64;%ODB%\libodb-sqlite\bin64;%ODB%\libodb-mysql\bin64;%ODB%\libodb-pgsql\bin64;%ODB%\libodb-oracle\bin64;%ODB%\libodb-mssql\bin64;%ODB%\libodb-boost\bin64;%ODB%\libodb-qt\bin64;%MYSQL64%\bin;%SQLITE%\bin64;%PGSQL64%\bin;%ORACLE64%;%QTCORE64%\bin;%PATH%

if "_%2_" == "__" goto end

%2 %3 %4 %5 %6 %7 %8 %9
goto end

:usage
echo.
echo usage: setenv.bat vc-version ...
echo.

:error
exit /b 1

:end
