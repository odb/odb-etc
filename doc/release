0. Pre-release build:

@@ Need -fPIC for Qt5 (-cxxflags "-fPIC")

  ./dist.sh -j 10 -cxxflags -fPIC -ccp gcc-5 -cxxp g++-5 -complete -test \
  -db sqlite -mssql-test-options --with-mssql-server=tcp:windows2008r2,1435 \
  2>&1 | tee dist.log

  cd ~/work/odb/build/mingw-w64
  ./build-dist -j 10 -rebuild -pack 2>&1 | tee build.log

1. Preparation

[ ] Test with the latest GCC snapshot.

    - For latest (6):

      apt-get install libmpfr-dev
      apt-get install libmpc-dev

      In new terminal:

      . ../from-source.txt
      ./configure ...
      make
      sudo make install

    - Build latest snapshot (see etc1/gcc/from-source.txt) and install in
      /usr/local. See previous snapshot build for configure command line.

    - Configure ODB manually in /tmp/odb and smoke-test

      Will need
      . from-source.txt
      export LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH

      Build one of the test directories with this ODB:

      make odb=/tmp/odb/odb/odb

      Then compare with the same directory built with the previous
      version of GCC to detect any changes in the generated code.
      To clear the directories, use:

      find . -name '*.o' -o -name '*.d' -o -name driver | xargs rm

    - Run (replace -5)

    ./dist.sh -j 22 -complete -test -db pgsql -mssql-test-options \
--with-mssql-server=tcp:windows2008r2,1435 \
-ccp gcc-5 -cxxp g++-5 -cxxpflags "-W -Wall -O3" \
-cc gcc-5 -cxx g++-5 -cxxflags "-W -Wall -Wno-unknown-pragmas" \
2>&1 | tee dist.log

    - Check dist.log for new warnings

    - Uninstall snapshot from /usr/local

[ ] Test with old/new versions of databases.

[ ] Grep through @@.

    cd ~/work/odb/odb
    grep -r '@@' odb | grep '@@' | grep -v forward | grep -v Wextra

    - Make sure new simple value pragmas are added to the map in pragma.cxx

    - Make sure newly added options support database prefix, if necessary.

[ ] Run srccheck.pl.

    In ODB compiler:

    ~/work/xsd/srccheck.pl

    In runtimes:

    ~/work/xsd/srccheck.pl --runtime

[ ] Make sure correct license headers are used in respective runtimes
    (search for GPL, NCUEL).

[ ] Update version information, dates in: version, version.hxx, man pages,
    manual. Grep the source code for the previous version.

[ ] Proofread NEWS file.

[ ] Update/add prerequisites in INSTALL files.

[ ] Check documentation: manual XHTML, manual PDF, man pages, usage.


2. Source, Binary Packages and Testing

[ ] Run tests and examples from the build tree:

    make clean
    make -j 22 test 2>&1 | tee build.log
    grep -i warning build.log

[ ] Run alternative configuration tests:

    * Other DB versions
    * Other compilers/versions

    C++11:

    make conf=c++11 clean
    make conf=c++11 -j 22 test 2>&1 | tee build.log
    grep -i warning build.log

    Qt5:

    make conf=qt5 clean
    make conf=qt5 -j 22 test 2>&1 | tee build.log
    grep -i warning build.log

    C++11 autotools (latest GCC):

    ./dist.sh -j 22 -complete -test -mssql-test-options \
--with-mssql-server=tcp:windows2008r2,1435 \
-ccp gcc-4.8 -cxxp g++-4.8 -cxxpflags "-W -Wall -O3" \
-cc gcc-4.8 -cxx g++-4.8 -cxxflags "-std=gnu++11 -W -Wall -O3 -Wno-unknown-pragmas" \
2>&1 | tee dist.log

    Qt5 autotools:

    @@ TODO

    Clang:

    ./dist.sh -j 22 -rebuild -test -mssql-test-options \
--with-mssql-server=tcp:windows2008r2,1435 -cc clang -cxx clang++ \
-cxxflags "-W -Wall -Wno-unknown-pragmas -O0" 2>&1 | tee dist.log

    Clang C++11:

    ./dist.sh -j 22 -rebuild -test -mssql-test-options \
--with-mssql-server=tcp:windows2008r2,1435 -cc clang -cxx clang++ \
-cxxflags "-std=c++11 -W -Wall -Wno-unknown-pragmas -O0" 2>&1 | tee dist.log

[ ] Run adhoc-test/multi-db

    On Linux:

    make type=static clean
    make type=static -j 16
    make type=static test

    make type=dynamic clean
    make type=dynamic -j 16
    make type=dynamic test

    make -f makefile.rdynamic clean
    make -f makefile.rdynamic -j 16
    make -f makefile.rdynamic test

    make -f makefile.dll clean
    make -f makefile.dll -j 16
    make -f makefile.dll test
    make -f makefile.dll clean

    On Windows (after building all the runtimes as part of VC10 test below):

    - Copy over the test including the generated code (makefile.dll)
    - Build the solution with VC10
    - On Linux: make -f makefile.dll test-prep
    - Open new Command Prompt, run setenv.sh, then cd to test-adhoc and
      run test.bat

[ ] Package and test distribution (result in /tmp/pack/):

    ./dist.sh -j 22 -complete -test -ccp gcc-4.5 -cxxp g++-4.5 \
    -mssql-test-options --with-mssql-server=tcp:windows2008r2,1435 \
    2>&1 | tee dist.log
    grep -i warning dist.log

[ ] Test UNIX install:

    * Watch out for disk space; build in $HOME to be able to uninstall *

    cd ~/odb/build/install
    ~/work/odb/etc/test/unix/build -rebuild -test -mssql-test-options \
--with-mssql-server=tcp:windows2008r2,1435 -j 22 2>&1 | tee build.log
    grep -i warning build.log

[ ] Test Embedded:

    Copy libodb, libodb-sqlite, and odb-examples .tar.gz packages
    from /tmp/pack to adhoc-tests/raspi.

    Update ODB versions in the versions file.

    In new terminal:

    cd ~/work/odb/adhoc-tests/raspi
    ./build-static

    On raspi:

    cd /mnt/tmp/raspi
    ./driver --database /tmp/static.db
    ls -l ./driver # check/update size growth; previous was 546416
                                                            540432 2.2.0
							    558632 2.3.0
							    566952 2.4.0
    On dev:

    ./build-shared

    On raspi:

    export LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH
    ./driver --database /tmp/shared.db
    ls -l * # check/update size growth; previous was 35348/38244/107532
                                                     35024/61356/115468  2.2.0
                                                     35504/87348/119792  2.3.0
						     35980/102896/123840 2.4.0
    On dev:

    ./clean

[ ] Build Windows binary (new terminal):

    cd ~/work/odb/build/mingw-w64
    ./build-dist -j 22 -rebuild -pack 2>&1 | tee build.log

[ ] Test on Windows with VC8:

    Connect drive T:

    In Cygwin terminal:
      cd ~/projects/odb
      ./unpack
      ./all 0 8 all all all 2>&1 | tee all.log
      grep -i warning all.log

[ ] Test on Windows with VC9:

    Connect drive T:

    In Cygwin terminal:
      cd ~/projects/odb
      ./unpack
      ./all 4 9 all all all 2>&1 | tee all.log
      grep -i warning all.log

[ ] Test on Windows with MinGW:

    @@ UNIX install test.
    @@ C++11 build (maybe).

    Connect drive T:

    In MSYS terminal (ODB distribution):

      cd /c/projects/odb/mingw
      ./unpack
      ./build -rebuild -test 2>&1 | tee build.log
      grep -i warning build.log

[ ] Test on Windows with VC10:

    Connect drive T:

    In Cygwin terminal:
      cd ~/projects/odb
      ./unpack

      ./all 5 10 all all Win32 2>&1 | tee all.log
      ./all 4 10 all all x64 2>&1 | tee all.log

      grep -i warning all.log

[ ] Run multi-db test on Windows (see above).

[ ] Test on Windows with VC11:

    Connect drive T:

    In Cygwin terminal:
      cd ~/projects/odb
      ./unpack
      ./all 4 11 all all all 2>&1 | tee all.log
      grep -i warning all.log

[ ] Test on Windows with VC12:

    Connect drive T:

    In Cygwin terminal:
      cd ~/projects/odb
      ./unpack
      ./all 5 12 all all all 2>&1 | tee all.log
      grep -i warning all.log

[ ] Build and Test Solaris x86 binary:

    cd ~/work/odb/build
    ./pack solaris
    cd /tmp
    scp -r pack-solaris-X.Y.Z sol86:

    ssh sol86
    cd pack-solaris-X.Y.Z
    ./build-dist -rebuild -arch i686-solaris2.10 2>&1 | tee build.log
    cd test

    ./build-test -rebuild -test -j 1 ./cc-i686-12u2 2>&1 | tee build.log
    grep -i warning build.log

    ./build-test -rebuild -test -j 1 ./cc-i686-12u2-stlport 2>&1 | tee build.log
    grep -i warning build.log

[ ] Build and Test Solaris SPARC binary:

    cd ~/work/odb/build
    ./pack solaris
    cd /tmp
    scp -r pack-solaris-X.Y.Z fire:

    ssh fire
    cd pack-solaris-X.Y.Z
    ./build-dist -rebuild -arch sparc-solaris2.10 2>&1 | tee build.log
    cd test
    ./build-test -rebuild -test -j 2 ./cc-sparc-12u2 2>&1 | tee build.log
    grep -i warning build.log


[ ] Build and Test MacOS X x86 binary:

    cd ~/work/odb/build
    ./pack darwin
    cd /tmp
    scp -r pack-darwin-X.Y.Z pbook:

    ssh mini
    cd pack-darwin-X.Y.Z
    ./build-dist -rebuild -arch i686-apple-darwin10 2>&1 | tee build.log

    ssh mini
    cd pack-darwin-X.Y.Z/test
    ./build-test -rebuild -test -j 1 ./gcc-qt 2>&1 | tee build.log
    grep -i warning build.log

    ssh rbook
    cd pack-darwin-X.Y.Z/test
    ./build-test -rebuild -test -j 2 ./clang-cxx11 2>&1 | tee build.log
    grep -i warning build.log

[ ] Build and Test GNU/Linux x86 binary:

    @@ Verify features.h is removed from include-fixed.

    cd ~/work/odb/build
    ./pack linux-gnu

    On sarge32:
      scp -r shuya:/tmp/pack-linux-gnu-X.Y.Z
      cd pack-linux-gnu-X.Y.Z

      # Watch out for -libexecdir if using history.
      #
      ./build-dist -rebuild -arch i686-linux-gnu 2>&1 | tee build.log
      scp test/odb-X.Y.Z-i686-linux-gnu.tar.bz2 shuya:/tmp/pack-linux-gnu-X.Y.Z/test/

    On shuya:
      cd /tmp/pack-linux-gnu-X.Y.Z/test
      ./build-test -rebuild -test -j 16 ./gcc 2>&1 | tee build.log
      grep -i warning build.log

[ ] Build and Test GNU/Linux x86-64 binary:

    @@ Verify features.h is removed from include-fixed.

    cd ~/work/odb/build
    ./pack linux-gnu

    On sarge64:
      scp -r shuya:/tmp/pack-linux-gnu-X.Y.Z
      cd pack-linux-gnu-X.Y.Z

      # Watch out for -libexecdir if using history.
      #
      ./build-dist -rebuild -arch x86_64-linux-gnu 2>&1 | tee build.log
      scp test/odb-X.Y.Z-x86_64-linux-gnu.tar.bz2 shuya:/tmp/pack-linux-gnu-X.Y.Z/test/

    On shuya:
      cd /tmp/pack-linux-gnu-X.Y.Z/test
      ./build-test -rebuild -test -j 16 ./gcc 2>&1 | tee build.log
      grep -i warning build.log

    On fedora17-64:
      @@ TODO test with mysql, pgsql, and sqlite, Use gcc-minimal or
      install Boost and Qt.

      In the meantime, at least test person.hxx/bad.hxx

[ ] Build .deb packages

    Copy standard linux packages to build/debian/, unpack them:

    tar xfj odb-*-i686-linux-gnu.tar.bz2
    tar xfj odb-*-x86_64-linux-gnu.tar.bz2

    Update etc/binary/debian/changelog with a new version. See if any new
    doc/ or etc/ file were added, update rules if so.

    Copy the etc/binary/debian directory to the binary directories.

    cd odb-X.Y.Z-x86_64-linux-gnu
    dpkg-buildpackage -us -uc -rfakeroot

    cd odb-X.Y.Z-i686-linux-gnu
    dpkg-buildpackage -us -uc -rfakeroot -ai386

    Test each package:

    alien -t -g odb_X.Y.Z-N.deb # make sure dir structure is ok

    On sarge:

    sudo dpkg -i odb_X.Y.Z-N.deb
    which odb
    odb --version
    odb -d mysql person.hxx
    odb -d mysql private.hxx
    man odb
    sudo dpkg -P odb

[ ] Build .rpm packages

    Save standard linux packages in /tmp/pack and rebuild them with
    the -libexec libexec option. Copy them to ~/rmp/SOURCES/

    Edit odb.spec, update version, changelog. See if any new doc/ or
    etc/ file were added. Make sure default.options Debian-specific
    part is still 6 lines long.

    cd etc/binary/redhat
    rpmbuild -ba --target x86_64-pc-linux-gnu odb.spec
    rpmbuild -ba --target i686-pc-linux-gnu odb.spec

    Result is in ~/rpm/RPMS/

    Test each package:

    alien -t -g odb-X.Y.Z-N.rpm # make sure dir structure is ok

    On CentOS (32) and Fedora (64):

    sudo rpm -i odb-X.Y.Z-N.rpm
    which odb
    odb --version
    odb -d mysql person.hxx
    odb -d mysql private.hxx
    man odb
    sudo rpm -e odb

[ ] Tag the new version:

    ./tag.sh X.Y.Z


3. Dependencies

[ ] Release all dependencies that have changes.


4. Publishing

[ ] Generate .sha1-sums, latest links for binaries and source distributions:

    cd ~/work/codesynthesis.com/public/download/odb/X.Y
    ../../../../procedures/odb-sha1.sh X.Y.Z

    Capture .sha1 sums for later announcements.
    cat `ls *.sha1 | sort`

    ../../../../procedures/odb-latest.sh X.Y.Z-N

[ ] Project page modifications:

    + overview (if major new features worth mentioning)
    + features
    + examples
    + license (if adding new database)
    + build instructions for UNIX/Windows
    + platforms and compilers

    Proofread changes.

[ ] Copy doc/ leaving only .css, .png, .xhtml, .ps, and .pdf files. Make
    the following modifications to the .xhtml files:

    1. Remove XML declaration (the first line).
    2. Add IE conditional CSS inclusion:

       <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="/default-ie.css"/><![endif]-->

[ ] Update download links in the download page. Add new packages if any.

[ ] Test sandbox.

[ ] Sync sandbox with production (do --dry-run first):

    cd ~/work/codesynthesis.com
    rsync -avz --copy-unsafe-links --dry-run public/ codesynthesis.com:/var/www/codesynthesis.com/public/

[ ] While uploading, prepare mailing list announcement.

[ ] Test production.

[ ] Announce on the odb-users, odb-announcement mailing list.

[ ] Write shorter version of the announcement and update the following pages
    and feeds:

    /products/odb/index.xhtml  (News box)
    /products/odb/news.rss
    /projects/index.xhtml      (News box)
    /index.xhtml
    /news.xhtml
    /news.rss

    Test sanbox, then sync with production.

5. Announcements

[ ] Update version/release date in Wikipedia: main page and ORM comparison.

[ ] Write a blog post.

[ ] Write and send external announcement. See seperate page for the list
    of places.

[ ] Update Wikipedia page with new major features, if any.
